import numpy as np

# Przyklad dla naszej grupy
# (1 - [0.1, 0.25])x <= [260,300]

def oblicz_wartosc_importu (wspolczynniki_lewa, y):
    x_opt = y[0] / wspolczynniki_lewa[0]
    y_opt = [a * x_opt for a in wspolczynniki_lewa]
    print(f"y_opt: {y_opt}")

    W = wspolczynniki_lewa[0] - wspolczynniki_lewa[1]
    print(f"W: {W}")

    X = (y[0] - y[1]) / W
    print(f"X: {X}")

    # import
    I = ((wspolczynniki_lewa[0] * y[1]) - (y[0] * wspolczynniki_lewa[1])) / W
    # print(f"Watosc jednostek importu: {I}")

    # Sprawdzenie
    yL = [wspolczynniki_lewa[0] * X + I, wspolczynniki_lewa[1] * X + I]
    # print(f"Sprawdzenie: {yL}")

    return I

def oblicz_Hposs(wspolczynniki, import_produktu, y):

    XL = (y[0] - import_produktu[0]) / wspolczynniki[0]
    XR = (y[1] - import_produktu[1]) / wspolczynniki[1]
    print(f"")

    XL_XR = [XL, XR]

    X_Poss = []
    for X in XL_XR:
        przedzial = [wspolczynniki[0] * X + import_produktu[0],
                     wspolczynniki[1] * X + import_produktu[1]]
        print(f"przedzial: ", przedzial)

        dlugosc_przedzialu_oryginalny = y[1] - y[0]
        dlugosc_przedzialu_zalozony = przedzial[1] - przedzial[0]
        Hposs = dlugosc_przedzialu_oryginalny / dlugosc_przedzialu_zalozony
        X_Poss.append(Hposs)
    return (X_Poss, XL_XR)


def oblicz_najlepszy_import(wspolczynniki, import_produktu, y, x):
    XL = wspolczynniki[0] * x + import_produktu
    XR = wspolczynniki[1] * x + import_produktu

    dlugosc_przedzialu_oryginalny = y[1] - y[0]
    dlugosc_przedzialu_zalozony = XR - XL
    Hposs = dlugosc_przedzialu_oryginalny / dlugosc_przedzialu_zalozony

    return Hposs


def oblicz_linspace(start, end, step):
    num_elements = int(np.round((end - start) / step)) + 1
    return np.linspace(start, end, num_elements)


def oblicz_hBest_xBest_yBest(linspaceX, linspaceI, wspolczynniki_lewa, y):
    h_best = 0
    x_best = 0
    i_best = 0
    for x in linspaceX:
        for i in linspaceI:
            h = oblicz_najlepszy_import(wspolczynniki_lewa, i, y, x)
            if (h > h_best):
                h_best = h
                x_best = x
                i_best = i

    return (h_best, x_best, i_best)
