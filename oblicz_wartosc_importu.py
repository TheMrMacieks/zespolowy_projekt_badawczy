import itertools

# Metoda lewy/prawy brzeg

# Przyklad dla naszej grupy
# (1 - [0.1, 0.25])x <= [260,300]

def wartosc_importu (wspolczynniki_lewa, y):
    x_opt = y[0] / wspolczynniki_lewa[0]
    y_opt = [a * x_opt for a in wspolczynniki_lewa]
    print(f"y_opt: {y_opt}")

    result = [a * b for a, b in zip(wspolczynniki_lewa, y)]

    combinations = itertools.product(wspolczynniki_lewa, y)

    # Szukamy przedzialu min/max
    multiplied_values = [a * b for a, b in combinations]
    min_value = min(multiplied_values)
    max_value = max(multiplied_values)


    W = wspolczynniki_lewa[0] - wspolczynniki_lewa[1]
    print(f"W: {W}")
    X = (y[0] - y[1]) / W

    print(f"X: {X}")

    # import
    I = ((wspolczynniki_lewa[0] * y[1]) - (y[0] * wspolczynniki_lewa[1])) / W

    # print(f"Watosc jednostek importu: {I}")

    # Sprawdzenie
    yL = [wspolczynniki_lewa[0] * X + I, wspolczynniki_lewa[1] * X + I]
    # print(f"Sprawdzenie: {yL}")

    return I


