from obliczenie_rownan import oblicz_Hposs, oblicz_hBest_xBest_yBest, oblicz_linspace, oblicz_wartosc_importu
import numpy as np

wspolczynniki_lewa = [0.75, 0.9]
y = [260, 300]
I1 = [55, 65]

if __name__ == "__main__":
    import_produktu = oblicz_wartosc_importu(wspolczynniki_lewa, y)
    print(f"Watosc jednostek importu: {import_produktu}")

    # [0.75, 0.9]x + [55, 65] <= [260, 300]
    Hposs = oblicz_Hposs(wspolczynniki_lewa, I1, y)
    XL_XR = Hposs[1]
    Hposs = Hposs[0]

    print(f"Hposs: ", Hposs)
    print(f"Granice XL_XR: ", XL_XR)

    linspaceX = oblicz_linspace(XL_XR[1], XL_XR[0], 0.1)
    linspaceI = oblicz_linspace(I1[0], I1[1], 0.1)

    h_best, x_best, i_best = oblicz_hBest_xBest_yBest(linspaceX, linspaceI, wspolczynniki_lewa, y)

    print(f"h_best: ", h_best)
    print(f"x_best: ", x_best)
    print(f"i_best: ", i_best)
